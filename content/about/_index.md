---
title: "About"
date: 2023-09-21T14:13:31-03:00
---

{{< figure src="/images/profile.jpg" class="profile-img" alt="A picture of me">}}
<!--more-->

## Hi there!
I'm **João**, a **Data Scientist** working at **[iFood](https://www.ifood.com.br/)**.  I'm also finishing my undergraduate studies in **Computer Engineering** at **[UFSCar](https://www.ufscar.br/)** (Federal University of São Carlos, São Paulo, Brazil).

I mainly work on building **recommender systems** to improve user experience in grocery shopping. My research interests involve **manifold learning and dimensionality reduction** algorithms. I'm also an enthusiast of **data visualization** and like to explore ways to better describe and find visual patterns in data.
